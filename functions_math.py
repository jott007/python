def add(a, b):
    print "ADDING %d + %d" % (a, b)
    return a + b

def substract(a, b):
    print "SUBSTRACTING %d - %d" % (a, b)
    return a - b

def multiply(a, b):
    print "MULTIPLYING %d * %d" % (a, b)
    return a * b

def devide(a, b):
    print "DEVIDING %d / %d" % (a, b)
    return a / b

print "Let's do some math with just functions!"

age = add (48, 4)
height = substract(205, 20)
weight = multiply(12, 8)
iq = devide(240, 2)

print "Age: %d, Height: %d, Weight: %d, IQ: %d" % (age, height, weight, iq)
print "This is a puzzle."

what = add (age, substract(height, multiply(weight, devide(iq,2))))

print "That becomes: ", what, "Can you do it by hand?"
