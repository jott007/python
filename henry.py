class Mammal(object):
  def breath(self):
    print "inhale and exhale"
    
class Cat(Mammal):
  def __init__(self, what):
    self.words = what
    
  def speak(self):
    print self.words

henry = Cat("meow, meeoow")
henry.breath()
henry.speak()
