class Song(object):
  def __init__(self, lyrics):
    self.songtext = lyrics
    
  def sing_me_a_song(self):
    for line in self.songtext:
      print line

happy_bday = Song(["Happy birthday to you",
                   "Happy birthday to you",
                   "Happy birthday, happy birthday",
                   "Happy Birthday to you"])

happy_bday.sing_me_a_song()
